package com.sunsetcast.app.sunsetcastmedia_app.ui;

import android.app.ActivityOptions;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.Toast;

import com.sunsetcast.app.sunsetcastmedia_app.models.Movie;
import com.sunsetcast.app.sunsetcastmedia_app.adapters.MovieAdapter;
import com.sunsetcast.app.sunsetcastmedia_app.adapters.MovieItemClickListener;
import com.sunsetcast.app.sunsetcastmedia_app.R;
import com.sunsetcast.app.sunsetcastmedia_app.models.Slide;
import com.sunsetcast.app.sunsetcastmedia_app.adapters.SliderPagerAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class HomeActivity extends AppCompatActivity implements MovieItemClickListener {

    private List<Slide> lstSlides ;
    private ViewPager sliderpager;
    private TabLayout indicator;
    private RecyclerView MoviesRV ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        sliderpager = findViewById(R.id.slider_pager) ;
        indicator = findViewById(R.id.indicator);
        MoviesRV = findViewById(R.id.Rv_movies);

        // prepare a list of slides ..
        lstSlides = new ArrayList<>() ;
        lstSlides.add(new Slide(R.drawable.the_great_muppet_caper,"The Great Muppet Caper"));
        lstSlides.add(new Slide(R.drawable.the_curse_of_la_llorona,"La LLorona"));
        lstSlides.add(new Slide(R.drawable.the_flintstones_in_viva_rock_vegas,"The Flintstones In Viva Rock Vegas"));
        lstSlides.add(new Slide(R.drawable.the_poison_rose,"The Poison Rose"));
        lstSlides.add(new Slide(R.drawable.the_day,"The Day"));
        SliderPagerAdapter adapter = new SliderPagerAdapter(this,lstSlides);
        sliderpager.setAdapter(adapter);
        // setup timer
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new HomeActivity.SliderTimer(),4000,6000);
        indicator.setupWithViewPager(sliderpager,true);

        // Recyclerview Setup
        // ini data

        List<Movie> lstMovies = new ArrayList<>();
        lstMovies.add(new Movie("Missing Link",R.drawable.missing_link,R.drawable.missing_link));
        lstMovies.add(new Movie("Ultra Music ",R.drawable.ultra_music_festival_miami_2017_day_1_alesso,R.drawable.ultra_music_festival_miami_2017_day_1_alesso));
        lstMovies.add(new Movie("Ed Sheeran",R.drawable.ed_sheeran_shape_of_you,R.drawable.ed_sheeran_shape_of_you));
        lstMovies.add(new Movie("The Flintstones",R.drawable.the_flintstones_in_viva_rock_vegas,R.drawable.the_flintstones_in_viva_rock_vegas));
        lstMovies.add(new Movie("Ultra Music",R.drawable.ultra_music_festival_miami_2017_day_1_armin_van_buuren,R.drawable.ultra_music_festival_miami_2017_day_1_armin_van_buuren));
        lstMovies.add(new Movie("The Boss Baby",R.drawable.the_boss_baby,R.drawable.the_boss_baby));

        MovieAdapter movieAdapter = new MovieAdapter(this,lstMovies,this);
        MoviesRV.setAdapter(movieAdapter);
        MoviesRV.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));





    }

    @Override
    public void onMovieClick(Movie movie, ImageView movieImageView) {
        // here we send movie information to detail activity
        // also we ll create the transition animation between the two activity

        Intent intent = new Intent(this,MovieDetailActivity.class);
        // send movie information to deatilActivity
        intent.putExtra("title",movie.getTitle());
        intent.putExtra("imgURL",movie.getThumbnail());
        intent.putExtra("imgCover",movie.getCoverPhoto());
        // lets crezte the animation
        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(HomeActivity.this,
                                                    movieImageView,"sharedName");

        startActivity(intent,options.toBundle());



        // i l make a simple test to see if the click works

        //Toast.makeText(this,"item clicked : " + movie.getTitle(),Toast.LENGTH_LONG).show();
        // it works great


    }

    class SliderTimer extends TimerTask {


        @Override
        public void run() {

            HomeActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (sliderpager.getCurrentItem()<lstSlides.size()-1) {
                        sliderpager.setCurrentItem(sliderpager.getCurrentItem()+1);
                    }
                    else
                        sliderpager.setCurrentItem(0);
                }
            });


        }
    }





}
