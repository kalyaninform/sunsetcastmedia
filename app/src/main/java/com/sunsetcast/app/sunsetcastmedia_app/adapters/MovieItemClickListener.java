package com.sunsetcast.app.sunsetcastmedia_app.adapters;

import android.widget.ImageView;

import com.sunsetcast.app.sunsetcastmedia_app.models.Movie;

public interface MovieItemClickListener {

    void onMovieClick(Movie movie, ImageView movieImageView); // we will need the imageview to make the shared animation between the two activity

}
